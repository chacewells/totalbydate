var gulp = require('gulp');
var argv = require('yargs').argv;
var git = require('gulp-git');
var sh = require('shelljs');

function handleErr (err) {
  if (err) {
    throw err;
  }
}

gulp.task('remote', function () {
  git.addRemote('origin', 'git@github.com:chacewells/totalbydate.git', function (err) {
    if (err) throw err;
  })
})

gulp.task('add', function () {
  sh.exec('git add *');
})

gulp.task('commit', function () {
  sh.exec('git commit -m "' + argv.m + '"');
})

gulp.task('push', function () {
  git.push('origin', 'master', handleErr);
})

gulp.task('syncup', ['add', 'commit', 'push']);

gulp.task('pull', function () {
  git.pull('origin', 'master', handleErr);
})
