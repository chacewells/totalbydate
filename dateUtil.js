require('datejs');

var openTimeString = "07:00:00",
    closeTimeString = "20:00:00",
    t11_30_a_str = "11:30:00",
    t11_31_a_str = "11:31:00",
    t04_30_p_str = "16:30:00",
    t04_31_p_str = "16:31:00",
    yyyy_MM_dd_format = "yyyy-MM-dd ";

function tzOffset (date) {
  return date.isDaylightSavingTime() ? "CDT" : "CST";
}

function getTime(date, todStr) {
  return Date.parse(date.toString(yyyy_MM_dd_format) + todStr + ' ' + tzOffset(date));
}

var dateUtil = {
  tzOffset: tzOffset,
  getTime: getTime,
  openTime: function _7_am (today) {
    return getTime(today, openTimeString);
  },
  closeTime: function _8_pm (today) {
    return getTime(today, closeTimeString);
  },
  _11_30_am: function _11_30_am (today) {
    return getTime(today, t11_30_a_str);
  },
  _11_31_am: function _11_31_am (today) {
    return getTime(today, t11_31_a_str);
  },
  _04_30_pm: function _04_30_pm (today) {
    return getTime(today, t04_30_p_str);
  },
  _04_31_pm: function _04_31_pm (today) {
    return getTime(today, t04_31_p_str);
  }
};

module.exports = dateUtil;
