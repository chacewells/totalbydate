var papa = require('papaparse'),
var Q = require('q');
fs = require('fs');
require('underscore');
require('datejs');

function sortByDate (recs) {
  return recs.sort(function (a,b) {
      var aSD = a['Sale Date'], bSD = b['Sale Date'];
      if (aSD > bSD) return 1;
      else if (aSD === bSD) return 0;
      else return -1;
  });
}

function filterByFOHComm (recs) {
  return recs.filter(function (rec) {
    return rec.Category.equalsIgnoreCase('FOH Comm');
  });
}

function toDateBuckets (recs) {
  var buckets = [],
  fmt = "yyyy-MM-dd";

  Q.fcall(function () {
    return recs;
  }).then(filterByFOHComm)
  .then(sortByDate)
  .then(function (filtered) {
    var current = recs[0];
    var bucket = [current];
    filtered.forEach(function (rec, idx) {
      if (idx === 0) return;
      if (rec['Sale Date'].toString(fmt) === current['Sale Date'].toString(fmt)) {
        bucket.push(rec);
      } else {
        buckets.push(bucket);
        bucket = [rec];
      }
    });
  }).done()

  return buckets;
}

var magpieFilename = "data/magpiecafe_sold_items_from_2015-07-27_to_2015-07-31.csv";
var magpieData = fs.readFileSync(magpieFilename).toString();

var magpiePapa = papa.parse(magpieData, {
  header: true
});

var filteredFOHCommSortedSalesDateAsc = magpiePapa.data.filter(function (rec) {
  return rec.Category === "FOH Comm";
}).map(function (rec) {
  rec['Sale Date'] = new Date(rec['Sale Date']);
  // rec['Sale Date'].setTimezone('CST');
  return rec;
}).sort(function (a, b) {
  var aSD = a['Sale Date'], bSD = b['Sale Date'];
  if (aSD > bSD) return 1;
  else if (aSD === bSD) return 0;
  else return -1;
});

module.exports = filteredFOHCommSortedSalesDateAsc;

var report = [];
